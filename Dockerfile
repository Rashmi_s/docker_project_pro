FROM python:3.6.9
RUN  python3 -m pip --no-cache install --upgrade pip
WORKDIR /app

COPY . .

RUN pip3 --no-cache-dir install -r requirements.txt
EXPOSE 5000
ENTRYPOINT ["python3"]

CMD ["app.py"]



